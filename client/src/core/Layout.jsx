import React, { Fragment } from "react";
// note that NavLink passes isActive directly does not need alternative settings
import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router";
import { isAuth, signout } from "../auth/helpers";

const Layout = ({ children }) => {
  const navigate = useNavigate();

  const nav = () => (
    <ul className="nav nav-tabs bg-primary">
      <li className="nav-item">
        <NavLink
          className={({ isActive }) =>
            isActive ? "text-dark nav-link active" : "text-light nav-link"
          }
          to="/"
        >
          Home
        </NavLink>
      </li>

      {!isAuth() && (
        <>
          <li className="nav-item">
            <NavLink
              className={({ isActive }) =>
                isActive ? "text-dark nav-link active" : "text-light nav-link"
              }
              to="/signin"
            >
              Signin
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              className={({ isActive }) =>
                isActive ? "text-dark nav-link active" : "text-light nav-link"
              }
              to="/signup"
            >
              Signup
            </NavLink>
          </li>
        </>
      )}

      {isAuth() && isAuth().role === "admin" && (
        <li className="nav-item">
          <NavLink
            className={({ isActive }) =>
              isActive ? "text-dark nav-link active" : "text-light nav-link"
            }
            to="/admin"
          >
            {isAuth().name}
          </NavLink>
        </li>
      )}

      {isAuth() && isAuth().role === "subscriber" && (
        <li className="nav-item">
          <NavLink
            className={({ isActive }) =>
              isActive ? "text-dark nav-link active" : "text-light nav-link"
            }
            to="/private"
          >
            {isAuth().name}
          </NavLink>
        </li>
      )}

      {isAuth() && (
        <li className="nav-item">
          <span
            className="nav-link"
            style={{ cursor: "pointer", color: "#fff" }}
            onClick={() => {
              signout(() => {
                navigate("/");
              });
            }}
          >
            Signout
          </span>
        </li>
      )}
    </ul>
  );

  return (
    <Fragment>
      {nav()}
      <div className="container">{children}</div>
    </Fragment>
  );
};

export default Layout;
