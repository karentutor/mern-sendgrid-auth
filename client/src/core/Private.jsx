import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import Layout from "../core/Layout";
import axios from "axios";
import {
  isAuth,
  getCookie,
  signout,
  updateUser,
  authenticate,
} from "../auth/helpers";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const Private = () => {
  const [values, setValues] = useState({
    buttonText: "Submit",
    initialName: "",
    updatedName: false,

    confirmPassword: "",
    initialEmail: "",

    role: "",
    udpatedEmail: false,
    updatedPassword: "",
    showPassForm: false,
    showUpdateForm: true,
  });

  const {
    buttonText,
    confirmPassword,
    initialEmail,
    initialName,
    updatedEmail,
    updatedName,
    updatedPassword,
    showPassForm,
    showUpdateForm,
  } = values;

  const token = getCookie("token");
  const navigate = useNavigate();

  useEffect(() => {
    loadProfile();
  }, []);

  const loadProfile = () => {
    axios({
      method: "GET",
      url: `${process.env.REACT_APP_API}/user/${isAuth()._id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => {
        //        console.log("PRIVATE PROFILE UPDATE", response);
        const { role, name, email } = response.data;
        setValues({ ...values, role, initialName: name, initialEmail: email });
      })
      .catch((error) => {
        //      console.log("PRIVATE PROFILE UPDATE ERROR", error.response.data.error);
        if (error.response.status === 401) {
          signout(() => {
            navigate("/");
          });
        }
      });
  };

  const validateEmail = (email) => {
    return email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
  };

  const handleChange = (name) => (event) => {
    // console.log(event.target.value);
    setValues({ ...values, [name]: event.target.value });
  };

  const submitEnterPassForm = (event) => {
    event.preventDefault();
    //authenticate password and send all updated data to the backend

    axios({
      method: "POST",
      url: `${process.env.REACT_APP_API}/user/pass-update`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        confirmPassword,
        updatedEmail,
        updatedName,
        updatedPassword,
      }, //old password
    })
      .then((response) => {
        updateUser(response, () => {
          //here we update in the callback
          setValues({
            ...values,
            buttonText: "Submit",
            showUpdateForm: true,
            showPassForm: false,
          });
          toast.success(
            "Profile updated successfully.  If you requested an email change, please click the link to verify email address and update in profile"
          );
        });
        // toast.success(
        //   `Hey ${response.data.user.name}, Your data has been updated`
        // save the response (user, token) localstorage/cookie
        // authenticate(response, () => {
        //   console.log('success');
        // });
      })
      .catch((error) => {
        console.log("SIGNIN ERROR", error.response.data);
        setValues({
          ...values,
          buttonText: "Submit",
          showUpdateForm: true,
          showPassForm: false,
        });
        toast.error(error.response.data.error);
      });

    // if succeeed

    // if the password fails -- offer again

    // if pass go forward

    // if fail again -- sign out
  };

  const submitUpdateForm = (event) => {
    event.preventDefault();

    //   error check if name blank return
    if (updatedName === "") {
      toast.error("Name is required");
      return;
    }

    // error check if email blank return
    if (updatedEmail === "") {
      toast.error("Email is required");
      return;
    }

    // if change email or pass go to enter pass form
    // requries a password if these values updated
    if (
      (updatedEmail && updatedEmail.length > 0) ||
      (updatedPassword && updatedPassword.length > 0)
    ) {
      // show password form and update page
      // go  to submit enterPassform to see what happens next in this if

      // check
      if (updatedEmail && !validateEmail(updatedEmail))
        return toast.error("Email must be a valid email");

      if (updatedPassword && updatedPassword.length < 8)
        return toast.error("Pass must be at least 8 characters");
      setValues({ ...values, showPassForm: true, showUpdateForm: false });
      // return is needed to halt further exectuion here
      return;
    }

    //continue with only name update
    setValues({ ...values, buttonText: "Submitting" });
    // update with new name or use original
    // here we continue in case they bypass and submit not by form
    // here empty onames will be caught by the server
    // password is not provided ... so do not need to check
    // here the server will check
    // check is here in validation
    let newName;
    if (updatedName || updatedName === "") newName = updatedName;
    else newName = initialName;

    let newEmail;
    if (updatedEmail || updatedEmail === "") newEmail = updatedEmail;
    else newEmail = initialEmail;

    // check for password is in server file
    axios({
      method: "PUT",
      url: `${process.env.REACT_APP_API}/user/update`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: { name: newName, email: newEmail, password: updatedPassword },
    })
      .then((response) => {
        //    console.log("PRIVATE PROFILE UPDATE SUCCESS", response);
        //update user will be used to update in local storage
        updateUser(response, () => {
          //here we update in the callback
          setValues({ ...values, buttonText: "Submitted" });
          toast.success("Profile updated successfully");
        });
      })
      .catch((error) => {
        //  console.log("PRIVATE PROFILE UPDATE ERROR", error.response.data.error);
        setValues({ ...values, buttonText: "Submit" });
        toast.error(error.response.data.error);
      });
  };

  const updateForm = () => (
    <form>
      {/* <div className="form-group">
        <label className="text-muted">Role</label>
        <input
          defaultValue={role}
          type="text"
          className="form-control"
          disabled
        />
      </div> */}
      <div className="form-group">
        <label className="text-muted">Name</label>
        <input
          onChange={handleChange("updatedName")}
          value={updatedName === "" || updatedName ? updatedName : initialName}
          type="text"
          className="form-control"
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Email</label>
        <input
          onChange={handleChange("updatedEmail")}
          value={
            updatedEmail === "" || updatedEmail ? updatedEmail : initialEmail
          }
          type="email"
          className="form-control"
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Password</label>
        <input
          onChange={handleChange("updatedPassword")}
          value={updatedPassword && updatedPassword}
          type="password"
          className="form-control"
        />
      </div>

      <div>
        <button className="btn btn-primary" onClick={submitUpdateForm}>
          Submit
        </button>
      </div>
    </form>
  );

  const passForm = () => (
    <form>
      <p className="lead text-center">Enter your password</p>
      <div className="form-group">
        <label className="text-muted">Password</label>
        <input
          onChange={handleChange("confirmPassword")}
          value={confirmPassword}
          type="password"
          className="form-control"
        />
      </div>
      <div>
        <button className="btn btn-primary" onClick={submitEnterPassForm}>
          {buttonText}
        </button>
      </div>
    </form>
  );

  return (
    <Layout>
      <div className="col-md-6 offset-md-3">
        <ToastContainer />
        <h1 className="pt-5 text-center">Private</h1>
        <p className="lead text-center">Profile update</p>
        {showUpdateForm && updateForm()}
        {showPassForm && passForm()}
      </div>
    </Layout>
  );
};

export default Private;
