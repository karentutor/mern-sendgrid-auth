import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import jwt from "jsonwebtoken";
import Layout from "../core/Layout";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { updateUser } from "./helpers";

const ResetEmail = () => {
  const [values, setValues] = useState({
    stateEmail: "",
    stateName: "",
    token: "",
  });

  const { stateEmail, stateName } = values;

  let tokenValue = useParams().token;

  useEffect(() => {
    let token = tokenValue;
    let { email, name } = jwt.decode(token);
    if (token) {
      axios({
        method: "PUT",
        url: `${process.env.REACT_APP_API}/reset-email`,
        data: { email, resetEmailLink: token },
      })
        .then((response) => {
          updateUser(response, () => {
            console.log("RESET EMAIL SUCCESS", response);
            setValues({ ...values, stateEmail: email, stateName: name });
          });
        })
        .catch((error) => {
          console.log("RESET PASSWORD ERROR", error.response.data);
          // toast.error(error.response.data.error);
          // setValues({ ...values, buttonText: "Reset password" });
        });
    }
  }, []);

  return (
    <Layout>
      {stateName && stateEmail ? (
        <>
          <h1 className="p-5 text-center">Success!</h1>
          <h3 className="p-5 text-center">
            Hi {stateName}, I have updated your email to {stateEmail}
          </h3>
        </>
      ) : (
        <>
          <h1 className="p-5 text-center text-danger">Error</h1>
          <h3 className="p-5 text-center"> sorry a fault</h3>
        </>
      )}
    </Layout>
  );
};

export default ResetEmail;
