import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import { useParams } from "react-router-dom";
import Layout from "../core/Layout";
import axios from "axios";
import jwt from "jsonwebtoken";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const Activate = () => {
  //show is for 'show form'
  const [values, setValues] = useState({
    name: "",
    token: "",
    show: true, // show form to start then hide
  });
  const { name, token, show } = values;

  let tokenValue = useParams().token;
  let navigate = useNavigate();

  useEffect(() => {
    // here we decode the name so that we can say 'hi'
    if (tokenValue) {
      let { name } = jwt.decode(tokenValue);
      setValues({ ...values, name, token: tokenValue });
    }
  }, [tokenValue]);

  const clickSubmit = (event) => {
    event.preventDefault();
    axios({
      method: "POST",
      url: `${process.env.REACT_APP_API}/account-activation`,
      data: { token },
    })
      .then((response) => {
        console.log("ACCOUNT ACTIVATION", response);
        setValues({ ...values, show: false });

        toast.success(response.data.message);
        //navigate("/signin"); -- will not see toast
      })
      .catch((error) => {
        console.log("ACCOUNT ACTIVATION ERROR", error.response.data.error);
        toast.error(error.response.data.error);
      });
  };

  // This is our form -- only activate the account -- only click on the button
  const activationLink = () => (
    <div className="text-center">
      <h1 className="p-5">Hey {name}, Ready to activate your account?</h1>
      <button className="btn btn-outline-primary" onClick={clickSubmit}>
        Activate Account
      </button>
    </div>
  );

  return (
    <Layout>
      <div className="col-md-6 offset-md-3">
        <ToastContainer />
        {show && activationLink()}
      </div>
    </Layout>
  );
};

export default Activate;
