import {Navigate} from 'react-router-dom';
import {isAuth} from '../auth/helpers';

const PrivateRoute = ({ children }) => {
    const auth = isAuth();
    return auth ? children : <Navigate to="/signin" />;
}

export default PrivateRoute;
  