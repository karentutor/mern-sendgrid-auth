import {Navigate} from 'react-router-dom';
import {isAuth} from '../auth/helpers';

const AdminRoute = ({ children }) => {
    const auth = isAuth() && isAuth().role === 'admin';
    return auth ? children : <Navigate to="/signin" />;
}

export default AdminRoute;
  